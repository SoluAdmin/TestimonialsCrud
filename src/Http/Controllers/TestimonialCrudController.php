<?php

namespace SoluAdmin\TestimonialsCrud\Http\Controllers;

use SoluAdmin\Support\Http\Controllers\BaseCrudController;
use SoluAdmin\TestimonialsCrud\Http\Requests\TestimonialRequest;

class TestimonialCrudController extends BaseCrudController
{

    public function setUp()
    {
        parent::setup();

        $this->crud->orderBy('lft');

        $this->crud->enableReorder('name', 1);
        $this->crud->allowAccess('reorder');
    }

    public function store(TestimonialRequest $request)
    {
        return parent::storeCrud($request);
    }

    public function update(TestimonialRequest $request)
    {
        return parent::updateCrud($request);
    }
}
