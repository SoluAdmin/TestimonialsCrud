<?php

namespace SoluAdmin\TestimonialsCrud\Http\Forms;

use SoluAdmin\Support\Interfaces\Form;

class TestimonialCrudForm implements Form
{
    public function fields()
    {
        return [
            [
                'label' => trans('SoluAdmin::TestimonialsCrud.name'),
                'name' => 'name',
                'type' => 'text'
            ],
            [
                'label' => trans('SoluAdmin::TestimonialsCrud.position'),
                'name' => 'position',
                'type' => 'text'
            ],
            [
                'label' => trans('SoluAdmin::TestimonialsCrud.text'),
                'name' => 'text',
                'type' => 'textarea'
            ],
        ];
    }
}
