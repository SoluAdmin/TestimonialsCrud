<?php

namespace SoluAdmin\TestimonialsCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class TestimonialCrudDataTable implements DataTable
{
    public function columns()
    {

        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::TestimonialsCrud.name')
            ],
            [
                'name' => 'position',
                'label' => trans('SoluAdmin::TestimonialsCrud.position')
            ],
            [
                'name' => 'text',
                'label' => trans('SoluAdmin::TestimonialsCrud.text'),
            ],
        ];
    }
}
