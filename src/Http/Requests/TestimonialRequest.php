<?php

namespace SoluAdmin\TestimonialsCrud\Http\Requests;

use Illuminate\Support\Facades\Auth;

class TestimonialRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [

        ];
    }
}
