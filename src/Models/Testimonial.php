<?php

namespace SoluAdmin\TestimonialsCrud\Models;

use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $protected = ['id'];

    protected $translatable = ['text', 'position'];
}
