<?php

namespace SoluAdmin\TestimonialsCrud\Providers;

use SoluAdmin\Support\Providers\CrudServiceProvider;
use SoluAdmin\Support\Helpers\PublishableAssets as Assets;

class TestimonialsCrudServiceProvider extends CrudServiceProvider
{
    protected $assets = [
        Assets::CONFIGS,
        Assets::TRANSLATIONS,
        Assets::VIEWS,
        Assets::MIGRATIONS,
        Assets::MODULE_MIGRATIONS,
    ];

    protected $resources = [
        'testimonial' => 'TestimonialCrudController'
    ];
}
