@module("SoluAdmin\\TestimonialsCrud")
    <li>
        <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.TestimonialsCrud.route_prefix', '') . '/testimonial') }}">
            <i class="fa fa-quote-right"></i> <span>{{trans('SoluAdmin::TestimonialsCrud.testimonial_plural')}}</span>
        </a>
    </li>
@endmodule