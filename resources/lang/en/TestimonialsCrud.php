<?php

return [
    'testimonial_singular' => 'Testimonial',
    'testimonial_plural' => 'Testimonials',
    'name' => 'Name',
    'position' => 'Position',
    'image' => 'Image',
    'text' => 'Text',
];
