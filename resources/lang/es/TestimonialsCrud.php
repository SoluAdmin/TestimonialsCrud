<?php

return [
    'testimonial_singular' => 'Testimonio',
    'testimonial_plural' => 'Testimonios',
    'name' => 'Nombre',
    'position' => 'Cargo',
    'image' => 'Imagen',
    'text' => 'Texto',
];
