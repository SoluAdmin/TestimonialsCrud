[![build status](https://gitlab.com/SoluAdmin/TestimonialsCrud/badges/master/build.svg)](https://gitlab.com/SoluAdmin/TestimonialsCrud/commits/master)

[![coverage report](https://gitlab.com/SoluAdmin/TestimonialsCrud/badges/master/coverage.svg)](https://gitlab.com/SoluAdmin/TestimonialsCrud/commits/master)

# TestimonialsCrud Module for SoluAdmin

Testimonials crud module for SoluAdmin

## Installation

To install through composer, simply put the following in your `composer.json` file:

```json
{
    "repositories": [
        {
          "type": "git",
          "url": "git@gitlab.com:SoluAdmin/TestimonialsCrud.git"
        }
      ],
    "require": {
        "SoluAdmin/TestimonialsCrud": "0.1.*"
    }
}
```

And then run `composer require SoluAdmin/TestimonialsCrud` from the terminal.

## Testing

To run the test
```bash
composer test
```

To create the test coverage report
```bash
composer test-coverage
```

To watch for change in files and run the test automatically, you should have [gulp](http://gulpjs.com/) globally installed and [nodejs](https://nodejs.org/) with [npm](https://www.npmjs.com/)
```
npm install
grunt
```

## To add git hooks

```bash
composer run git-hooks
```

## Code Coverage

Report is found in
https://soluadmin.gitlab.io/TestimonialsCrud/
